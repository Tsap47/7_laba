#pragma once
#include <iostream>
const int I = 100;
const int J = 100;
void Read(int& i, int& j, int matrix[I][J]);
bool PositiveNumber(int& i, int& j, int matrix[I][J]);
bool Include_3_or_5(int& i, int& j, int matrix[I][J]);
int MinToSquare(int& i, int& j, int matrix[I][J]);
void Write(int& i, int& j, int matrix[I][J]);

