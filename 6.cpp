﻿/*6 лабораторка 5 вариант
 Дана целочисленная матрица {Aij}i=1...n;j=1..n , n<=100. Если все диагональные элементы матрицы положительны и содержат цифры 3 или 5,
 заменить минимальные элементы столбцов на суммы квадратов элементов соответствующих столбцов.*/
#include <iostream>
#include "Functions.hpp"
using namespace std;
int main()
{
	setlocale(LC_ALL, "Russian");
	int i;
	int j;
	int matrix[I][J];
	Read(i, j, matrix);
	if (PositiveNumber(i, j, matrix) == true && Include_3_or_5(i, j, matrix) == true)
	{
		MinToSquare(i, j, matrix);
		cout << "Все условия выполнены. Новая матрица:" << endl;
		Write(i, j, matrix);
	}
	else
	{
		cout << "Указанные условия не выполнены. Исходная матрица:" << endl;
		Write(i, j, matrix);
	}
	return 0;
}
